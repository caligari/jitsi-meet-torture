package cat.floss.jgt.test;

import org.jitsi.meet.test.base.JitsiMeetUrl;
import org.jitsi.meet.test.base.ParticipantType;
import org.jitsi.meet.test.web.WebParticipant;
import org.jitsi.meet.test.web.WebParticipantOptions;
import org.jitsi.meet.test.web.WebTestBase;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class MassiveClients
    extends WebTestBase
{
    private static final String INPUT_VIDEO_FILE = "resources/fakeVideoStream.y4m";
    private static final String INPUT_AUDIO_FILE = "resources/fakeAudioStream.wav";

    public static final String CONFERENCES_PNAME = "cat.floss.jgt.conferences";
    public static final int CONFERENCES_DEFAULT = 1;
    
    public static final String ROOM_NAME_PNAME = "cat.floss.jgt.room_name";
    public static final String ROOM_NAME_DEFAULT = "test";
    
    public static final String PARTICIPANTS_PNAME = "cat.floss.jgt.participants";
    public static final int PARTICIPANTS_DEFAULT = 4;
    
    public static final String SPEAKERS_PNAME = "cat.floss.jgt.speakers";
    public static final int SPEAKERS_DEFAULT =1;
    
    public static final String ENABLE_P2P_PNAME = "cat.floss.jgt.enable_p2p";
    public static final boolean ENABLE_P2P_DEFAULT = false;
    
    public static final String DURATION_PNAME = "cat.floss.jgt.duration";
    public static final int DURATION_DEFAULT = 120;

    int speakers;
    int listeners;
    
    public MassiveClients() 
    {    
        speakers = 0;
        listeners = 0;
    }
    
    private synchronized String nextSpeakerName()
    {
        speakers++;
        return "Speaker " + speakers;
    }
    
    private synchronized String nextListenerName()
    {
        listeners++;
        return "Listener " + listeners;
    }
    
    @DataProvider(name = "dp", parallel = true)
    public Object[][] createData(ITestContext context)
    {
        // If the tests is not in the list of tests to be executed,
        // skip executing the DataProvider.
        if (isSkipped()) { return new Object[0][0]; }

        String numConferencesP = System.getProperty(CONFERENCES_PNAME);
        int numConferences = numConferencesP == null ? CONFERENCES_DEFAULT : Integer.valueOf(numConferencesP);
        
        String roomName = System.getProperty(ROOM_NAME_PNAME);
        if (roomName == null) { roomName = ROOM_NAME_DEFAULT; }

        String numParticipantsP = System.getProperty(PARTICIPANTS_PNAME);
        int numParticipants = (numParticipantsP == null ? PARTICIPANTS_DEFAULT : Integer.valueOf(numParticipantsP));
        
        String speakersP = System.getProperty(SPEAKERS_PNAME);
        int speakers = (speakersP == null ? SPEAKERS_DEFAULT : Integer.valueOf(speakersP));

        String durationP = System.getProperty(DURATION_PNAME);
        int duration = (durationP == null ? DURATION_DEFAULT : Integer.valueOf(durationP));

        String enableP2pStr = System.getProperty(ENABLE_P2P_PNAME);
        boolean enableP2p = enableP2pStr == null ? ENABLE_P2P_DEFAULT :Boolean.parseBoolean(enableP2pStr);

        // Use one thread per conference.
        context.getCurrentXmlTest().getSuite()
            .setDataProviderThreadCount(numConferences);

        print("will run with:");
        print("conferences="+ numConferences);
        print("room_name=" + roomName);
        print("participants=" + numParticipants);
        print("speakers=" + speakers);
        print("duration=" + duration);
        print("enable_p2p=" + enableP2p);

        Object[][] ret = new Object[numConferences][4];
        for (int i = 0; i < numConferences; i++)
        {
            JitsiMeetUrl url = participants.getJitsiMeetUrl()
                .setRoomName(numConferences == 1 ? roomName : roomName + (i + 1))
                .appendConfig("config.p2p.useStunTurn=true")
                .appendConfig("config.disable1On1Mode=false")
                .appendConfig("config.testing.noAutoPlayVideo=true")
                .appendConfig("config.p2p.enabled=" + enableP2p);
            
            ret[i] = new Object[] { url, numParticipants, duration, speakers};
        }

        return ret;
    }

    @Test(dataProvider = "dp")
    public void testMain(
            JitsiMeetUrl url,
            int numberOfParticipants, 
            long duration, 
            int speakers)
        throws InterruptedException
    {
        Thread[] runThreads = new Thread[numberOfParticipants];

        for (int i = 0; i < numberOfParticipants; i++)
        {
            runThreads[i] = runAsync(i, url, duration, i >= speakers);            
            Thread.sleep(1000);
        }

        for (Thread t : runThreads)
        {
            if (t != null) { t.join(); }
        }
    }

    private Thread runAsync(int i, JitsiMeetUrl url, long testSeconds, boolean muted) {

        JitsiMeetUrl _url = url.copy();

        Thread joinThread = new Thread(() -> {

            WebParticipantOptions ops = new WebParticipantOptions();
            ops.setName(muted ? nextListenerName() : nextSpeakerName());
            ops.setParticipantType(ParticipantType.chrome);
            ops.setFakeStreamAudioFile(INPUT_AUDIO_FILE);
            
            _url.appendConfig("config.startWithVideoMuted=" + muted);
            _url.appendConfig("config.startWithAudioMuted=" + muted);

            WebParticipant participant = participants.createParticipant("web.participant" + (i + 1), ops);
            participant.joinConference(_url);

            long joinSeconds = nowSeconds();
            boolean userIsInConference = false;
            
            try
            {
                while (nowSeconds() - joinSeconds < testSeconds)
                {
                    if (!userIsInConference && participant.isInMuc())
                    {
                        try
                        {
                            Thread.sleep(1000); // time to allow
                            participant.setDisplayName(ops.getName());
                            userIsInConference = true;
                        }
                        catch (org.openqa.selenium.WebDriverException _e)
                        {
                            print("Warning: Slowly interface loading for participant " + ops.getName() + "...");
                        }
                    }
                    
                    // sleep a second
                    Thread.sleep(1000);                    
                }
            }
            catch (InterruptedException e)
            {
                throw new RuntimeException(e);
            }
            finally
            {
                participant.hangUp();
                closeParticipant(participant);
            }
        });

        joinThread.start();

        return joinThread;
    }

    private long nowSeconds() {
        return System.currentTimeMillis() / 1000;
    }

    private void print(String s)
    {
        System.err.println(s);
    }

    /**
     * {@inheritDoc}
     */
    public boolean skipTestByDefault()
    {
        // Skip by default.
        return true;
    }
}
